# ParTCP Documentation

This documentation is written in [reStructuredText][1] and built with the
[Sphinx Documentation Generator][2]. It makes use of the [Sphynx PlantUML extension][4]
and the [Sphynx Book theme][5].

The final pages can be found [here][3].

To build the HTML files call the following command from the project directory:

```html
$ env/bin/sphinx-build -b html source build
```

The `plantuml.jar` file must exist in directory `~/bin`. If it has another location
adapt the `plantuml` configuration option in `source/conf.py` accordingly.


[1]: https://docutils.sourceforge.io/rst.html
[2]: https://www.sphinx-doc.org
[3]: https://partcp.codeberg.page
[4]: https://chiplicity.readthedocs.io/en/latest/Using_Sphinx/UsingGraphicsAndDiagramsInSphinx.html#id19
[5]: https://sphinx-book-theme.readthedocs.io/en/latest

