DSGVO-Konformität
=================

Da es eine eindeutige Verbindung zwischen einem Datensatz in der
Mitgliederliste und einer ParTCP-Teilnehmerkennung gibt, müssen diese
Kennungen als personenbezogene Daten im Sinne der DSGVO betrachtet
werden. Daher müssen alle Teilnehmer über die Einzelheiten der
Datenverarbeitung informiert werden und dieser Verarbeitung ausdrücklich
zustimmen.

Das ParTCP-Konzept sieht die Möglichkeit vor, dass auf dem
Schlüsselserver eine Datenschutzerklärung und der Wortlaut der
Einwilligung hinterlegt wird, die von den Teilnehmern erwartet wird.
Eine *registration*-Nachricht (siehe Abschnitt „Wahlprotokoll >
Registrierung“) wird in diesem Fall nur dann verarbeitet, wenn sie das
Attribut *Consent-Statement* enthält und der Wert dieses Attributs exakt
mit dem Wortlaut der Einwilligung übereinstimmt, die auf dem Server
hinterlegt ist.

Da alle *registration*-Nachrichten auf dem Server gespeichert bleiben,
ist auf diese Weise für jeden Teilnehmer dauerhaft dokumentiert, dass er
in die Verarbeitung seiner personenbezogenen Daten eingewilligt hat.

