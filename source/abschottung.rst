Abschottungsprozedur
--------------------

Im Folgenden wird eine beispielhafte Prozedur beschrieben, mit der sich
eine Maschine abschotten lässt, um als „vertrauenswürdig“ zu gelten.

Klärung der Verantwortung
~~~~~~~~~~~~~~~~~~~~~~~~~

Die Personen, die mit der Einrichtung und Abschottung einer Maschine
betraut werden, müssen über entsprechende fachliche Qualifikationen
verfügen und das Vertrauen der Gemeinschaft genießen, in deren Auftrag
sie handeln. Die Einrichtung sollte von drei, muss aber mindestens von
zwei Personen durchgeführt werden. Diese sollten aus einer größeren
Gruppe von Personen, denen die Gemeinschaft per Abstimmung ihr Vertrauen
ausgesprochen hat, per Losverfahren bestimmt werden.

Die beauftragten Personen müssen während der gesamten Dauer der
nachfolgend beschriebenen Einrichtung anwesend sein und die im folgenden
beschriebenen Schritte durchführen bzw. begleiten. Jede Person bestätigt
am Ende durch eine eidesstattliche Versicherung, dass die Abschottung
des Servers ordnungsgemäß erfolgte.

Neben den technisch verantwortlichen Personen sollte die Gemeinschaft
mindestens eine weitere vertrauenswürdige Person als Beobachter
bestimmen. Diese soll den ordnungsgemäßen Ablauf der Einrichtung
überwachen und nach Möglichkeit auf Video dokumentieren.

Zufallsentscheide
~~~~~~~~~~~~~~~~~

Die nachfolgende Prozedur fordert an manchen Stellen, dass per Zufall
oder Los eine von mehreren Möglichkeiten gewählt wird. Um dies
umzusetzen, sollte jede beauftragte Person einen eigenen Würfel
mitbringen. Das Losverfahren sieht dann folgendermaßen aus:

1. Die verfügbaren Möglichkeiten werden mit 0 (!) beginnend
   durchnummeriert.
2. Die Personen würfeln nacheinander so oft mit ihren jeweiligen Würfeln,
   wie es der Anzahl an Möglichkeiten entspricht.
3. Die Augenzahlen aller Würfe aller Personen werden aufaddiert.
4. Die Summe wird durch die Anzahl der Möglichkeiten dividiert; der
   dabei verbleibende Rest gibt an, welche Möglichkeit ausgelost wurde.

Auswahl und Aufstellen der Maschine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Wenn auf einer Maschine geheime Abstimmungen und Wahlen durchgeführt
werden sollen, kommt für den Produktivbetrieb nur ein dedizierter Server
in Frage, auf dem keine anderen Anwendungen laufen. Die Verwendung von
virtuellen oder gemeinsam genutzten Servern ist lediglich für
Entwicklungs- und Testzwecke zulässig.

Um auszuschließen, dass die Hardware oder das BIOS der Maschine
manipuliert sind, muss es sich um ein handelsübliches Modell handeln,
das fabrikneu und originalverpackt von den Einrichtern gemeinsam in
Betrieb genommen wird. Idealerweise wird die Maschine aus einem Pool von
mindestens drei gleichwertigen Modellen per Los ausgewählt.

Vor der Inbetriebnahme ist das Gehäuse zu öffnen und das Innenleben der
Maschine in hoher Auflösung zu fotografieren. Die Fotos werden als Teil
der Dokumentation archiviert.

Der Server muss über eine schnelle Internet-Anbindung und eine feste
IP-Adresse verfügen, deshalb kommt als Aufstellort in der Regel nur ein
Rechenzentrum in Frage. Dieses sollte sorgfältig ausgewählt werden und
nach Möglichkeit über ein ISO-9001-zertifiziertes Qualitätsmanagement
verfügen. Im Zweifelsfall muss es den Nachweis bringen können, dass sich
während der gesamten Laufzeit niemand Unbefugtes physischen Zugang zu
der Maschine verschaffen konnte.

Installation des Betriebssystems
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Wenn die Maschine bereits ab Werk mit einem Betriebssystem ausgestattet
wurde, ist die gesamte Festplatte zu löschen. Anschließend ist ein
Betriebssystem zu installieren, das die technischen Voraussetzungen für
den ParTCP-Einsatz erfüllt.

Die Installation wird nicht über das Netzwerk, sondern direkt an der
Maschine von einem USB-Stick aus durchgeführt. Die Maschine ist dafür
mit Monitor und Tastatur auszustatten, und zwar auf eine Weise, dass die
Kabelverbindungen zwischen der Maschine und den angeschlossenen Geräten
stets für alle beteiligten Personen auf einen Blick sichtbar sind. Per
Losverfahren wird festgelegt, wer die Tastatur bedient. Die übrigen
Einrichter stellen sich so auf, dass sie sehen können, was getippt und
was auf dem Bildschirm angezeigt wird.

Die Einrichter haben sich davon zu überzeugen, dass die Maschine während
der Installation noch nicht mit dem Netzwerk verbunden ist. Wenn im
Laufe der Installationsprozedur das Rootpasswort für die Maschine
einzugeben ist, tippt jeder Einrichter nacheinander eine nur ihm
bekannte Zeichenfolge (mindestens acht Zeichen) ein. Dabei muss
sichergestellt sein, dass keine andere Person und keine
Überwachungskamera sehen kann, was getippt wird.

Der für die Installation verwendete USB-Stick muss von einem
vertrauenswürigen Distributor produziert und originalverpackt
angeliefert worden sein. Im Idealfall wird aus einem Pool von mindestens
drei Sticks, die aus unterschiedlichen, aber durchweg vertrauenswürdigen
Quellen stammen, einer ausgelost.

Die Einrichter ermitteln und notieren sich die Seriennummern der
wichtigsten Hardware-Komponenten des Servers: Hauptplatine,
Festplatte(n) und Netzwerkadapter.

Das Betriebssystem wird nach der Installation so angepasst, dass
Wartungsupdates regelmäßig automatisch durchgeführt werden. Das heißt,
es wird ein Prozess eingerichtet, der in bestimmten Zeitabständen eine
nicht-interaktive Updateroutine mit Rootrechten ausführt. Ferner ist
sicherzustellen, dass die üblichen Aufräumskripte des Systems
automatisch ausgeführt werden.

Es wird ein Benutzerkonto mit dem Namen „public“ und dem Passwort
„public“ angelegt und sichergestellt, dass über dieses Konto ein
SSH-Zugang und ein Lesezugriff auf das gesamte Dateisystem möglich ist.
Dieses Benutzerkonto darf über kein Home-Verzeichnis verfügen und auch
sonst kein Schreibrecht für irgendein Verzeichnis haben.

Installation und Test der Anwendung
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Nachdem das Betriebssystem installiert ist, werden Bildschirm und
Tastatur entfernt, der Server wird mit dem Netzwerk verbunden und neu
gestartet. Die weiteren Schritte erfolgen von einem virtuellen Terminal
aus, wobei auch hier wieder ausgelost wird, wer die Tastatur bedient und
wer beobachtet. Die Maschine muss bis zum Ende der Abschottung weiterhin
im Sichtfeld der Einrichter bleiben.

Die Verbindung zum Server wird im Namen des Rootbenutzers mittels SSH
hergestellt, wobei die Passworteingabe wieder geteilt erfolgt. Die
Teilung stellt nicht nur sicher, dass keine Einzelperson mit Rootrechten
auf den Server zugreifen kann. Das erfolgreiche Einloggen ist auch
zugleich die Bestätigung dafür, dass die Anmeldung am „richtigen“, das
heißt an dem zuvor eingerichteten Server erfolgt. Als zusätzliche
Absicherung werden auch noch einmal die Seriennummern der
Hardware-Komponenten mit den zuvor notierten verglichen.

Durch entsprechende Shell-Kommandos werden die Anwendungen installiert,
die für den Einsatz der ParTCP-Software erforderlich sind, insbesondere
Apache/PHP und Git. Anschließend erfolgt die Installation der
ParTCP-Software gemäß Dokumentation, das heißt durch Clonen aus dem
Original-Repository. Die ParTCP-Konfigurationsdatei ist so anzupassen,
dass Sie den Gegebenheiten auf der Maschine und den Wünschen der
Gemeinschaft entspricht. Der Apache-Server ist so zu konfigurieren, dass
er nur Dateien aus den vorgesehenen Verzeichnissen ausliefert, und die
Inhalte dieser Verzeichnisse dürfen ohne Rootrechte nicht mehr
veränderbar sein.

Nach der Installation sendet jeder Einrichter von einem persönlichen
Rechner aus einige ParTCP-Ping-Nachrichten an den Server und testet
dabei insbesondere auch die kryptografischen Funktionen. Anschließend
wird das ParTCP-Schlüsselverzeichnis geleert (damit beim nächsten Aufruf
neue Schlüssel erzeugt werden). Unmittelbar danach wird die
Netzwerkverbindung getrennt.

Abschottung und Versiegelung
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Nun wird der Server abgeschottet und versiegelt. Abschotten heißt, dass
Zugriffe mit Rootrechten unterbunden werden, selbst für den Fall, dass
jemand physischen Zugang zu der Maschine hat. Wie dies im Einzelnen
geschieht, hängt vom verwendeten Betriebssystem ab und liegt in der
Verantwortung der Einrichter. Die folgenden Schritte sind lediglich als
Anregung zu verstehen:

-  persönliche Benutzerkonten entfernen, sofern vorhanden
-  sicherstellen, dass kein Benutzer Mitglied der *sudo*-Gruppe ist
-  Root-Passwort entfernen bzw. durch ein Zufallspasswort ersetzen [1]_
-  Booten in Single-User-Mode durch Vergabe eines Zufallspassworts o. ä.
   unmöglich machen [2]_
-  Konsolenzugang sperren: physisch durch Unbrauchbarmachen der
   Schnittstellen (Sekundenkleber) und/oder softwartechnisch durch
   Löschen der erforderlichen Gerätetreiber

Nach dem Abschotten ist das Servergehäuse so mit Sicherheitsaufklebern
und Plomben zu versehen, dass es nicht mehr möglich ist, das Gehäuse zu
öffnen oder die unbrauchbar gemachten Schnittstellen zu nutzen, ohne
irreversible Spuren zu hinterlassen.

Die Maschine wird an ihren endgültigen Aufstellort gebracht,
angeschlossen und hochgefahren. Jeder Einrichter sendet von einem
persönlichen Rechner aus eine ParTCP-Ping-Nachricht an den Server und
notiert sich den zurückgelieferten Public-Key. Anschließend loggt er
sich mit dem öffentlichen SSH-Zugang auf dem Server ein, vergleicht noch
einmal die Hardware-Seriennummern und prüft, ob der auf der Festplatte
abgelegte Public-Key mit dem ausgelieferten übereinstimmt.

Bericht
~~~~~~~

Jeder Einrichter fertigt einen Bericht an, in dem er die durchgeführten
Schritte im Detail dokumentiert. In dem Bericht sind die
Hardware-Seriennummern und der Aufstellort der Maschine zu notieren,
ferner die Sicherheitscodes der Siegelaufkleber und Plomben, die
IP-Adresse, unter der die Maschine erreichbar ist, der Public-Key sowie
Name und Passwort des öffentlichen SSH-Zugangs und das Credential, mit
dem sich der Hauptadministrator auf dem Server registrieren kann. Auf
der letzten Seite ist an Eides statt zu versichern, dass die Angaben in
dem Bericht vollständig sind und der Wahrheit entsprechen und dass
weitere Rootzugriffe auf den Server nach bestem Wissen und Gewissen
ausgeschlossen sind.

Die Berichte werden dem Auftraggeber ausgehändigt, und jeder Einrichter
legt eine Kopie seines eigenen Berichts dauerhaft in einem persönlichen
Archiv ab. Der Auftraggeber veröffentlicht den Bericht auf eine Weise,
die allen, auch künftig dazukommenden Mitgliedern der Gemeinschaft einen
Zugriff auf den Inhalt erlaubt.

.. [1]
   | https://www.tecmint.com/disable-root-login-in-linux
   | https://www.linuxcloudvps.com/blog/how-to-enable-and-disable-root-login-in-ubuntu
   | https://www.linuxfordevices.com/tutorials/linux/enable-disable-root-login-in-linux

.. [2]
   | https://www.techrepublic.com/article/how-to-password-protect-the-grub-boot-loader-in-ubuntu
   | https://kifarunix.com/how-to-protect-single-user-mode-with-password-in-ubuntu-18-04
   | https://www.linuxquestions.org/questions/linux-software-2/howto-disable-single-user-mode-544529
