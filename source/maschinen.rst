Vertrauenswürdige Maschinen
===========================

Das ParTCP-Sicherheitskonzept setzt voraus, dass es sich bei den
eingesetzten Servern um „vertrauenswürdige Maschinen“ handelt.
Vertrauenswürdig bedeutet in diesem Zusammenhang:

1. Die Maschine muss für alle Mitglieder einer Gemeinschaft transparent
   („gläsern“) sein. Das heißt, jedes Mitglied muss jederzeit in der
   Lage sein, das gesamte Dateisystem in Augenschein zu nehmen, um sich
   selbst ein Bild von der installierten Software und den auf der
   Maschine gespeicherten Daten zu machen.
2. Außer über das Absetzen von Nachrichten darf es keine Möglichkeit
   geben, Änderungen am Software- oder Datenbestand der Maschine
   vorzunehmen. Das heißt, es müssen hard- und softwareseitig
   Vorkehrungen getroffen werden, um Rootzugriffe und Manipulationen am
   Dateisystem zu verhindern. Dies wird als „Versiegeln“ oder
   „Abschotten“ bezeichnet.
3. Ein besonders hohes Maß an Vertrauenswürdigkeit lässt sich erreichen,
   wenn alle relevanten Daten auf einem WORM-Laufwerk (write once, read
   multiple) gespeichert werden, das heißt auf einem Laufwerk, das
   physikalisch nur das Erstellen von Dateien und Verzeichnissen
   erlaubt, aber nicht das nachträgliche Ändern. Leider weisen solche
   Laufwerke in der Regel nur geringe Schreibgeschwindigkeiten auf, so
   dass sie zwar für Archivierungszwecke, aber nicht für den
   Echtzeiteinsatz geeignet sind. Ungeachtet dessen bildet der
   Grundsatz, dass einmal geschriebene Daten nicht mehr verändert
   werden, einen Kern des ParTCP-Konzepts, so dass künftige technische
   Fortschritte bei den Speichermedien unmittelbar genutzt werden
   können.

Transparenz
-----------

Die Forderung nach Transparenz wird durch Einhaltung der folgenden
Bedingungen erfüllt:

-  Der Server wird mit einem quelloffenen Betriebssystem ausgestattet,
   dessen Quellcode unter einer öffentlich zugänglichen
   Versionsverwaltung steht. Der installierte Systemstand muss jederzeit
   mit offiziellen Paketquellen abgeglichen werden können.
-  Das Dateisystem des Servers kann von jedem Mitglied der Gemeinschaft
   in Augenschein genommen werden, z. B. durch einen SSH-Zugang, der
   Lesezugriff auf alle Dateien und Verzeichnisse bietet. Unzugänglich
   sind nur die privaten Schlüssel des Servers. Um eine übermäßige
   Belastung des Servers zu vermeiden, darf der Lesezugriff zeitlich
   und/oder mengenmäßig begrenzt werden.
-  Alle für die demokratischen Entscheidungsprozesse relevanten Daten
   müssen in Form von Klartextdateien im Dateisystem abgelegt sein. Der
   Einsatz von binär-codierten Dateien, insbesondere von
   Datenbanktabellen, ist nur für Indizierungs- oder Caching-Zwecke
   zulässig, um Datenzugriffe zu beschleunigen. In diesem Fall müssen
   die Routinen, die aus den Klartextdateien die Tabellen erzeugen bzw.
   füllen, in Form von lesbaren Skripten vorliegen (siehe nächsten
   Punkt).
-  Die Anwendungslogik muss durch Skripte festgelegt sein, die in einer
   gängigen Skriptsprache verfasst sind, unter öffentlich zugänglicher
   Versionsverwaltung stehen und auf definierte und dokumentierte Weise
   zum Server übertragen werden. Es muss jederzeit nachvollziehbar sein,
   welchem Versionsstand der Server entspricht und ob bzw. inwieweit ein
   Skript auf der Maschine gegenüber der Originalquelle verändert wurde.
-  Programme und Bibliotheken, die ausschließlich als Maschinencode
   vorliegen und nicht zum Betriebssystem gehören, dürfen nur eingesetzt
   werden, wenn der zugehörige Quellcode veröffentlicht ist, unter
   Versionskontrolle steht und direkt auf der Maschine kompiliert werden
   kann. Der Kompilierungsvorgang ist in diesem Fall zu dokumentieren.
   Updates dürfen nur durch Aktualisieren und Rekompilieren des
   Quellcodes eingespielt werden.

Abschottung
-----------

Die Forderung nach Abschottung wird durch Einhaltung folgender
Bedingungen erfüllt:

-  Alle äußeren Schnittstellen, die für eine Dateneingabe in Frage
   kommen, aber nicht für den Nachrichtenaustausch benötigt werden, sind
   ausgebaut oder unbenutzbar gemacht. Dies betrifft insbesondere
   serielle Schnittstellen für den Anschluss von Tastaturen und
   Datenträgern, aber zum Beispiel auch Bluetooth-, WLAN- und
   HDMI-Adapter.
-  Das Servergehäuse ist so zu verschließen und zu versiegeln, dass es
   sich nicht mehr öffnen lässt, ohne Spuren zu hinterlassen. Es muss
   jederzeit zweifelsfrei belegbar sein, dass das Innere des Gehäuses
   dem physischen Zustand entspricht, in dem es sich zum Zeitpunkt der
   Abschottung befunden hat.
-  Das Betriebssystem ist in einen Zustand zu versetzen, der das Lesen
   der privaten Schlüssel und das Überschreiben relevanter Daten an der
   Rechteprüfung der ParTCP-Software vorbei unmöglich macht. Welche
   Schritte dazu im Einzelnen gehören, hängt vom Betriebssystem und von
   der ParTCP-Implementierung ab. Beispiele wären:

   -  Sperrung aller Ports, außer denen, die für den
      Nachrichtenaustausch (z. B. HTTP) und die Lesezugriffe (z. B. SSH)
      benötigt werden
   -  Deaktivieren des Root-Benutzerkontos
   -  Deaktivieren des Bootloaders
   -  Deaktivieren des Single-User-Modus'
   -  Löschen von Treibern, die das Hochfahren des Servers von einer
      lokalen Konsole erlauben
   -  Sicherstellen, dass der Systembenutzer, in dessen Namen die
      ParTCP-Software ausgeführt wird (und der allein die Leserechte für
      die privaten Schlüssel sowie die Schreibrechte für das
      ParTCP-Datenverzeichnis besitzt), keine interaktiven Sitzungen
      starten oder zur Ausführung von Fremdprogrammen veranlasst werden
      kann

Die Abschottung muss von mindestens zwei, besser drei Personen
durchgeführt und schriftlich dokumentiert werden. Die Dokumentation ist
mit einer eidesstattlichen Versicherung der beteiligten Personen zu
versehen, in der diese „nach bestem Wissen und Gewissen“ bescheinigen,
dass es keine Möglichkeit mehr gibt, unbemerkt Änderungen an der
Maschine vorzunehmen. Die Dokumentation ist so zu veröffentlichen, dass
alle Mitglieder der Gemeinschaft darauf zugreifen können. Weitere
Einzelheiten finden sich im Abschnitt `Abschottungsprozedur`.
