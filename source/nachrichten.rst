Nachrichten
===========

Im ParTCP-Kontext bezeichnet der Begriff „Nachricht“ ein serialisiertes
Datenobjekt im YAML-Format, das zwischen Client und Server ausgetauscht
wird. Die Attribute dieses Datenobjekts werden als „Nachrichtenelemente“
oder kurz „Elemente“ bezeichnet. Damit ein YAML-String als Nachricht
angesehen wird, muss er ein Element mit dem Namen *Message-Type*
enthalten, das angibt, um welche Art von Nachricht es sich handelt.
Hiervon hängt ab, welche weitere Elemente erwartet werden und wie die
Nachricht verarbeitet wird.

Der Name eines Nachrichtenelements wird stets mit großen
Anfangsbuchstaben geschrieben. Besteht er aus mehreren Wörtern, werden
diese durch Bindestriche miteinander verbunden und jeweils mit großen
Anfangsbuchstaben geschrieben. Enthält ein Element ein Datenobjekt,
werden dessen Attribute in Kleinbuchstaben und mit Unterstrichen als
Worttrenner geschrieben.

Bestimmte Nachrichtenelemente sind unabhängig vom Nachrichtentyp für
bestimmte Zwecke reserviert:

:Message-Type: Typ der Nachricht

:To: Adressat (Empfänger) der Nachricht

:From: Absender der Nachricht

:Signature: digitale Signatur der Nachricht

:Date: Zeitstempel der Nachricht

:Public-Key: universeller öffentlicher Schlüssel des Absenders

:Original-Message: die ursprüngliche Nachricht als String (bei
	Antworten)

Ist der Inhalt eines Elements verschlüsselt, wird dem Namen eine Tilde
(~) angehängt. Enthält eine Nachricht dasselbe Element in
verschlüsselter und unverschlüsselter Form, wird das unverschlüsselte
Element ignoriert, es sei denn, das verschlüsselte Element ist nicht
entschlüsselbar.

Eine Nachricht, die ein Client an einen Server sendet, wird von diesem
wiederum mit einer Nachricht beantwortet, wobei die ursprüngliche
Nachricht im Normalfall in die Antwort eingebunden wird (Element
*Original-Message*). Hat die ursprüngliche Nachricht die Änderung eines
Datenobjekts ausgelöst, wird das neue Objekt vollständig in die Antwort
eingebunden, und die Antwort wird auf dem Server abgelegt.

Nachrichten werden grundsätzlich in UTF-8-Kodierung erstellt.
Zeilenumbrüche müssen durch Linefeed-Zeichen (LF, ASCII-Wert 10)
repräsentiert werden. Carriage-Return-Zeichen (CR, ASCII-Wert 13) sind
nicht zulässig, auch nicht in der Kombination CR+LF.
