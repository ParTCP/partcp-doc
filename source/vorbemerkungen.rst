Vorbemerkungen
==============

Dieses Dokument beschreibt die technischen Einzelheiten der
ParTCP-Plattform und insbesondere der Abstimmungsfunktionen. Es soll
interessierten Personen ermöglichen, die Sicherheit der Plattform zu
beurteilen, ohne sich mit den Implentierungsdetails zu befassen.

Begriffe
--------

Im Folgenden sind die Begriffe erklärt, die im Zusammenhang mit ParTCP
eine besondere Bedeutung haben oder in einem engeren Sinne verwendet
werden als im allgemeinen Sprachgebrauch üblich.

Eine **Gemeinschaft** (community) ist eine Gruppe von Menschen, die den
Wunsch haben oder aus äußeren Gründen gezwungen sind, gemeinsame
Entscheidungen auf demokratischer Grundlage zu treffen. Die Menschen,
die zu einer bestimmten Gemeinschaft gehören, werden als deren
**Mitglieder** (members) bezeichnet. Mitglieder, die an einem
ParTCP-Server registriert sind, heißen **Teilnehmer** (participants).

Eine Gemeinschaft kann in **Gruppen** gegliedert sein, die vorübergehend
oder dauerhaft eigene Gemeinschaften bilden.

Eine **Mitgliederliste** ist eine Liste oder Datenbank, in der die
Menschen verzeichnet sind, die Mitglied der Gemeinschaft sind oder zu
einem früheren Zeitpunkt waren. Die Informationen, die in der Liste zu
einem bestimmten Mitglied gespeichert sind, bilden den
**Mitgliedsdatensatz**. Die Mitgliederliste ist nicht Teil der
ParTCP-Plattform, sondern wird von einem externen System bereitgestellt.
Sie muss nicht zwangsläufig in digitaler Form vorliegen, sondern es kann
sich auch um Karteikarten, Aufzeichnungen auf Papier o. ä. handeln.

Ein **Schlüsselserver** ist ein zentraler Rechner, den eine Gemeinschaft
betreibt, um die Identitäten ihrer Mitglieder zu verwalten. Eine
Gemeinschaft kann auch mehrere Schlüsselserver betreiben, wenn es in der
Mitgliederliste eindeutige und unveränderliche Kriterien gibt, aus denen
abgeleitet werden kann, welcher Schlüsselserver für ein bestimmtes
Mitglied zuständig ist.

Eine **Identität** (identity) ist eine Kombination aus einer
Teilnehmerkennung und einem damit verknüpften öffentlichen Schlüssel.
Auf einem bestimmten Schlüsselserver kann es nicht mehrere Identitäten
mit derselben Teilnehmerkennung geben. Zu unterscheiden sind **offene**
(public) Identitäten, die mehr oder weniger nachvollziehbar mit einer
realen Person verknüpft sind, und **anonyme** (anonymous) Identitäten,
die so erstellt werden, dass eine solche Verknüpfung nicht möglich ist.

Eine **Registrierung** (registration) bezeichnet den Vorgang, bei dem
für ein Mitglied einer Gemeinschaft eine offene Identität auf dem
Schlüsselserver erstellt wird.

Ein **Administrator** ist ein Mitglied der Gemeinschaft, das berechtigt
ist, die Mitgliederliste einzusehen und Registrierungen durchzuführen.

Ein **Abstimmungsserver** ist ein zentraler Rechner, der zur Abwicklung
von Veranstaltungen und Abstimmungen einer Gemeinschaft dient. Ein
Abstimmungsserver kann zugleich Schlüsselserver sein, und umgekehrt.

Eine **Veranstaltung** (event) ist ein zeitlich befristeter
organisatorischer Rahmen mit festem Teilnehmerkreis, innerhalb dessen
eine oder mehrere Abstimmungen stattfinden. Alle Teilnehmer einer
Veranstaltung sind grundsätzlich berechtigt, an allen Abstimmungen
dieser Veranstaltung teilzunehmen. Werden abgestufte Berechtigungen
benötigt, müssen für die einzelnen Teilnehmerkreise jeweils eigene
Veranstaltungen definiert werden.

Jede Veranstaltung hat mindestens einen **Veranstaltungsleiter** (event
manager), der die Berechtigung hat, Abstimmungen zu definieren, zu
starten und zu beenden, Teilnehmer einzuladen etc.

Eine **Abstimmung** (voting) ist ein Vorgang, bei dem die Teilnehmer
einer Veranstaltung eine Liste von Optionen vorgelegt bekommen und diese
mit einer Bewertung versehen. Der **Abstimmungstyp** (voting type)
bestimmt darüber, welche Bewertungsmöglichkeiten zur Verfügung stehen
und wie aus den Einzelbewertungen am Ende das Abstimmungsergebnis
berechnet wird. Das heißt, auch Wahlen und Konsensierungen zählen im
ParTCP-Jargon zu den Abstimmungen.

Ein **Client** ist ein Computerprogramm, mit dessen Hilfe ein Mitglied
mit Schlüssel- und Abstimmungsservern kommunizieren kann. Der Client
stellt Funktionen zur Verfügung, um Schlüsselpaare zu erzeugen und zu
verwalten, Nachrichten zu verschlüsseln und zu signieren und digitale
Stimmzettel auszufüllen. Ein Client kann außerdem spezielle Funktionen
für Administratoren und Veranstaltungsleiter bereitstellen.

Eine **Nachricht** ist eine Zeichenkette im YAML-Format, die zum
Datenaustausch zwischen Clients und Servern dient. Einzelheiten hierzu
sind im Abschnit „Nachrichten“ zu finden.

Eine **vertrauenswürdige Maschine** ist ein Computersystem, das von
vertrauenswürdigen Personen nach festgelegten Regeln eingerichtet und
gegen nachträgliche Veränderungen abgeschottet wurde (siehe den
Abschnitt „Vertrauenswürdige Maschinen“).

Sequenzdiagramme
----------------

Dieses Dokument nutzt Sequenzdiagramme, um Prozesse zu visualisieren.
Dabei werden die beteiligten Akteure (Personen oder Computer) als
Spalten dargestellt und die Daten, die zwischen ihnen ausgetauscht
werden als Pfeile zwischen den Spalten. Durchgezogene Pfeile bedeuten,
dass es sich um einen Datenaustausch innerhalb der ParTCP-Plattform
handelt; gestrichelte Pfeile bezeichnen einen nicht näher definierten
Kommunikationsweg.

Wenn der Datenaustausch in Form einer ParTCP-Nachricht stattfindet, ist
der Nachrichtentyp in Fettschrift angegeben, und darunter werden in
Klammern die wichtigsten Nachrichtenelemente aufgezählt.

Wenn in der Spalte „Mitglied“ der weiße Aktivitätsbalken durch einen
grauen Aktivitätsbalken überlagert wird, bedeutet dies, dass das
Mitglied Nachrichten nicht unter seiner offenen Identität versendet,
sondern eine anonyme Identität nutzt.

Eine allgemeine Einführung in diese Art der Visualisierung gibt der
`Wikipedia-Artikel <https://de.wikipedia.org/wiki/Sequenzdiagramm>`_ zum
Stichwort „Sequenzdiagramm“.

Beispielnachrichten
-------------------

Im Anschluss an die Sequenzdiagramme werden jeweils die wichtigsten der
darin verwendeten Nachrichtentypen in Form von Beispielnachrichten
wiedergegeben. Aus Gründen der Lesbarkeit sind hier die Signaturen und
Zeitstempel weggelassen und die Inhalte mancher Nachrichtenelemente
gekürzt wiedergeben. Die gekürzten Stellen sind jeweils durch drei
Auslassungspunkte (…) gekennzeichnet.
